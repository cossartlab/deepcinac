------------
Dependencies
------------

deepCINAC has the following minimum requirements, which must be installed before you can get started using PyNWB.

#. Python 3.6, or 3.7
#. pip

deepCINAC has been tested on Ubuntu 18.04.1 LTS, Windows 10 and macOS Mojave, using Python 3.6

------------
Installation
------------


Install release from PyPI
-------------------------

The Python Package Index (PyPI) is a repository of software for the Python programming language. 

To install or update deepCINAC distribution from PyPI simply run:

.. code::

   $ pip install deepcinac

This will not automatically install the required dependencies. You can download our requirements.txt file and run
:

.. code::

   $ pip -r requirements.txt

The following packages will be installed :

- numpy
- scanimage-tiff-reader
- tifffile
- keras
- matplotlib
- Pillow
- scipy
- networkx
- seaborn
- alt_model_checkpoint
- hdf5storage
- PyYAML
- h5py
- read-roi

Make sure your setuptools package is up to date, you might otherwise get this error message during installation:
"ImportError: cannot import name ‘find_namespace_packages’ from ‘setuptools"

Two other packages will still be missing:

- **shapely**

On Linux or MacOs simply run:

.. code::

    $ pip install shapely

For windows users, follow the instruction  `there <https://pypi.org/project/Shapely/>`_.
If you went on the wheels option, here are the instruction to install the wheel file: first open a console
then cd to where you’ve downloaded your file and use:

.. code::

    $ pip install Shapely‑1.6.4.post2‑cp37‑cp37m‑win_amd64.whl


- **tensorflow**

If you wish to use the GPU to increase the significantly the speed of prediction, then install tensorflow-gpu instead.
For using the GPU, you will also need to install the NVIDIA CUDA Toolkit and Driver.

Here is a `link <http://penseeartificielle.fr/installer-facilement-tensorflow-gpu-sous-windows/>`_ to guide you to install it on windows (for french speakers).

For linux users, those links might be useful: `nvidia <https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html>`_ website and a `blog post <https://medium.com/@taylordenouden/installing-tensorflow-gpu-on-ubuntu-18-04-89a142325138>`_.


Use the notebook with google colab to infer neuronal activity
-------------------------------------------------------------

If you just want to infer neuronal activity of your calcium imaging data
and you don't possess a GPU or don't want to go through the process of configuring your environment to make use of it,
you can run this `notebook <https://gitlab.com/cossartlab/deepcinac/tree/master/demos/notebooks/demo_deepcinac_predictions.ipynb>`_
using `google colab <https://colab.research.google.com>`_.

Google provides free virtual machines for you to use: with about 12GB RAM and 50GB hard drive space, and TensorFlow is pre-installed.

You will need a google account. Upload the notebook on google colab, then just follow the instructions in the notebook to go through.
