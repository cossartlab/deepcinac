=================
Training tutorial
=================

Using the annotated .cinac files created with the GUI, you can now train your classifier.

Below are the few lines of codes needed to train the classifier:

.. code::

    cinac_model = CinacModel(results_path="/media/deepcinac/results",
                             using_splitted_tiff_cinac_movie=False,
                             n_epochs=20, batch_size=8)
    cinac_model.add_input_data_from_dir(dir_name="/media/deepcinac/data/cinac_ground_truth/for_training")
    cinac_model.prepare_model()
    cinac_model.fit()


First, you need to build an instance of CinacModel (see below for the parameters), the add data,
prepare the model and finally fit it to your data to train the classifier.

Input data are the cinac files, you can either load all files in a directory
or load files one by one.

To train your classifier to infer neuronal activity, you would have had to add onsets and peaks through the segments
recorded in the .cinac files using the GUI.

To train your classifier to predict cell type, you would have had to annotate the cell type of the segments's cell
recorded in the .cinac files using the GUI.

--------------------
CinacModel arguments
--------------------

By changing the arguments passed to CinacModel init() method, you can change most of the parameters used to train
the classifier. Including precising if you want to train a classifier to infer neuronal activity or cell type.

Let's see first the mandatory one, with no default value:

* **results_path**: path of the directory where to save the output files.

The other arguments are:

* **cell_type_classifier_mode**: (bool, default False) if False, means you want to train a classifier to infer neuronal activity,
  if True a cell type classifier.

* **using_splitted_tiff_cinac_movie**: (bool, default is True) Should be set to False on first use.
  In some cases, if multiple GPUs are used or depending on
  operating systems, it seems to be a thread lock issue when reading .cinac file (hdf5 format). A solution to fix it,
  if to keep this variable to True, it will then split the calcium imaging on individual tiff file (one by frame), in
  the directory given by the 'tiffs_dirname' argument. Used only for neuronal activity classifier.
  The individual tiffs frame will be created when adding data to the model, an exception will be raised when the tiffs
  will have been created, the code needs to be re-run then.

* **tiffs_dirname**: (str, default is None) path where to save the the calcium imaging movie as individual tiff for
  each frame.

* **n_epochs**: (int, default is 30), number of epochs for training the classifier

* **batch_size**: (int, default is 8) size of the batch used to train the classifier

* **split_values**: (tuple of 3 floats, default is (0.8, 0.2, 0)), tuple of 3 floats, the sum should be equal
  to 1. Give the distribution of the dataset into training, validation and test

* **window_len**: (int, default 100) number of frames of the temporal segments given to the classifier

* **max_width**: (int, default 25) number of pixels for the width of the frame surrounding the cell. Should be
  big enough so most of the overlaping cell fit in for activity classifier.

* **max_height**: (int, default 25) number of pixels for the height of the frame surrounding the cell. Should be
  big enough so most of the overlaping cell fit in for activity classifier.

* **model_descr**:  (str, default is ''), string describing the model, the string will be added to the name of the
  files used to save the model

* **with_augmentation_for_training_data**: (bool, default is True): is True, then geometric transformations are
  applied to the dataset

* **overlap_value**: (float, default 0.9), overlap between 2 segments using sliding window of size window_len
  0.9 means contiguous segments will share 90% of their frames. (temporal data augmentation)

* **max_n_transformations**: (int, default is 6) max number of geometric transformations to apply to each
  segment. (data augmentation)

* **n_gpus**: (int, default is 1) Maximum number of processes to spin up when using process-based threading.

* **workers**: (int, default is 10), number of workers used to run the classifier

* **cell_type_categories_yaml_file**: (str, default is None) path and filename of the yaml file used to
  configure the classifier for cell type (types of cell, number of classes). If None, then
  cell_type_categories_default.yaml will be used with pyramidal cells and interneuron and 2 classes.

* **n_windows_len_to_keep_by_cell**: (int, default 2), used only for cell type classifier, indicate how many
  segments of length window_len to keep for training. If too many segment are given then the classifier might
  not be able to generalize well

* **frames_to_avoid_for_cell_type**: (sequence of int, default []), list of frame indices. If given, then
  segment than contains one of those indices won't be add to the training. Useful for example if movies are
  concatenated.

* **using_multi_class**: (int, default is 1) number of classes used to classify the activity.
  So far only 1 or 3 are valid options. 3 means we distinguish active cell, from overlap activity,
  from neuropil and other. 1 class gives better results so far.
  Don't use it for cell type, the number of classes will be set through the yaml configuration file.

* **pixels_around**: (int, default is 0), number of pixels to add around the mask of the cell (for activity
  classifier)

* **buffer**: (int, default is 1): indicated of how many pixels to inflate the mask of the cell.

* **loss_fct**: (str, default is 'binary_crossentropy' if using_multi_class is 1, 'categorical_crossentropy'
  else), loss function used to train the classifier.

* **with_learning_rate_reduction**: (bool, default is True), if True, means the learning rate will be reduced
  according to the following arguments

* **learning_rate_reduction_patience**: (int, default is 2) number of epochs before reducing the learning
  rate if the validation accuracy has not improved, with_learning_rate_reduction needs to be True

* **learning_rate_start**: (float, default is 0.001) default learning rate to start with

* **with_early_stopping**: (bool, default is True) if True, then early stopping is activated, if the classifier doesn't
  progress for a given number of epochs indicated through the arg early_stop_patience

* **early_stop_patience**: (int, default is 15): number of epochs before the training stops if no progress is
  made (based on validation accuracy values)

* **with_shuffling**: (bool, default True), if True, the segments will be shuffle before added to training or
  validation dataset

* **seed_value**: (int, default is 42), if not None, used as seed for the shuffling of segments, giving a seed
  means the shuffle will always be the same for a given dataset.

* **main_ratio_balance**: (tuple of 3 floats, default is (0.6, 0.2, 0.2)), sequence of 3 float, the sum should
  be equal to 1. Used for activity classifier, precise the proportion in the final training dataset
  between sequence according to real transient, fake transient and "neuropil"

* **crop_non_crop_ratio_balance**: (tuple of 2 floats, default is (-1, -1)), use for stratification in activity
  classifier, if values are -1, we don't use it. Otherwise allows to balance segments between segment
  with transient cropped versus not cropped.

* non_crop_ratio_balance: (tuple of 2 floats, default is (-1, -1)), used for stratification in activity
  classifier, if values are -1, we don't use it. Otherwise allows to balance segments between segment
  with one transient versus more than one transient.

* **with_model_check_point**: (bool, default is True) allows to save weights of the classifier at each epoch

* **verbose**: (int, default is 2), 0 no verbose, 1 main outputs printed, 2 all outputs printed

* **dropout_value**: (float, default 0.5), dropout between CNN layers

* **dropout_value_rnn**: (float, default 0.5), dropout between RNN layers

* **dropout_at_the_end**: (float, default 0), dropout value at the end of the model

* **with_batch_normalization**: (bool, default False), if True use batch normalization

* **optimizer_choice**: (str, default is "RMSprop"), optimizer to use, choices "SGD", "RMSprop","Adam"

* **activation_fct**: (str, default is "swish") , activation function to use, you can choose any activation
  function available in TensorFlow, swish is also available.

* **without_bidirectional**: (bool, default is False), if True, LSTM is not bidirectional

* **conv_filters**: (tuple of 4 int, default is (64, 64, 128, 128)), the dimensionality of the output space
  (i.e. the number of filters in the convolution) sequence of 4 integers,
  representing the filters of the 4 convolution layers

* **lstm_layers_size** (sequence of int, default is (128, 256)), number of LSTM layer and size of each layer.

* **bin_lstm_size** (int, default is 256), size of the LSTM layer used in bin et al.

* **use_bin_at_al_version** (bool, default is True), using model inspired from the paper from Bin et al.

* **apply_attention**: (bool, default is True), if True, attention mechanism is used

* **apply_attention_before_lstm**: (bool, default is True), if True it means attention mechanism will be apply
  before LSTM

* **use_single_attention_vector** (bool, default is False), if True us single attention vector


----------------
Training outputs
----------------

The output files will be saved in the directory indicated by the argument "results_path" of CinacModel.

* **.json file**: representing the model used (its architecture). It is needed to run the classifier
  to infer neuronal data.

* **.h5 files**: representing the weights of each epoch. You can use the weights of one epoch to run the classifier on
  your data in association with the model file.

* **metrics_history.npz**: If the code run up till the last epoch parametrized (of if early stop is activated),
  a npz file will be saved with the metrics history at each epoch. Each key represents a metric and the value is a
  1d array.

---------
Demo code
---------

We provide demo code that shows you how to use DeepCINAC either as a notebook (that you can run on google colab)
or in a Python file.

**On google colab**

If  you don't possess a GPU or don't want to go through the process of configuring your environment to make use of it,
you can run this `notebook <https://gitlab.com/cossartlab/deepcinac/-/blob/master/demos/notebooks/demo_deep_cinac_training.ipynb>`_
using `google colab <https://colab.research.google.com>`_.

Google provides free virtual machines for you to use: with about 12GB RAM and 50GB hard drive space, and TensorFlow is pre-installed.

You will need a google account. Upload the notebook on google colab, then just follow the instructions in the notebook to go through.

Note that with google colab you won't probably be able to train an efficient classifier has the run time is limited to 12h. However, it will let you test the code.
You'll then need a local GPU or HPC access to train it with enough data to get good results.

**On your local device**

You can follow the steps described in this `demo file <https://gitlab.com/cossartlab/deepcinac/-/blob/master/demos/general/demo_deepcinac_training.py>`_.

To know the list of all the arguments, check the `API reference <https://deepcinac.readthedocs.io/en/latest/autoapi/deepcinac/cinac_model/index.html#deepcinac.cinac_model.CinacModel>`_.

