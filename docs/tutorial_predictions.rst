====================
Predictions tutorial
====================

Inferring neuronal activity
---------------------------

The classifier takes as inputs the motion corrected calcium imaging movie and spatial footprints of the sources (cells).

The outputs are float values between 0 and 1 for each frame and each source,
representing the probability for a cell to be active at that given frame.

The classifier we provide was trained to consider a cell as active during the rise time of its transients.

For more information, check-out our demo code.

**On google colab**

you can run this `notebook <https://gitlab.com/cossartlab/deepcinac/-/blob/master/demos/notebooks/demo_deepcinac_predictions.ipynb>`_.


**On your local device**

You can follow the steps described in this `demo file <https://gitlab.com/cossartlab/deepcinac/-/blob/master/demos/general/demo_deepcinac_predictions.py>`_.



Predicting cell type
--------------------

The classifier takes as inputs the motion corrected calcium imaging movie and spatial footprints of the sources (cells).

The outputs are float values between 0 and 1 for each cell type,
representing the cell type probability of a given cell.

We have trained a classifier on two cell type interneurons and pyramidal cells.
For training, interneurons were identified using GadCre mouse while pyramidal cell were putative.

A .yaml file allows to configure the cell types you want to use.

We are currently improving the classifier.

For more information, check-out our demo code.

**On google colab**

you can run this `notebook <https://gitlab.com/cossartlab/deepcinac/-/blob/master/demos/notebooks/demo_deepcinac_predictions.ipynb>`_.


**On your local device**

You can follow the steps described in this `demo file <https://gitlab.com/cossartlab/deepcinac/-/blob/master/demos/general/demo_deepcinac_predictions.py>`_.
